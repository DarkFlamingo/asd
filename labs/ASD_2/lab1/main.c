#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#define KNRM "\x1B[0m"
#define KRED "\x1B[31m"
#define KGRN "\x1B[32m"
#define KYEL "\x1B[33m"
#define KBLU "\x1B[34m"
#define KMAG "\x1B[35m"
#define KCYN "\x1B[36m"
#define KWHT "\x1B[37m"
#define RESET "\x1B[0m"

bool Parity(int num)
{
    return !(num % 2);
}

int FindMax(int length, int array[length])
{
    int Max = array[0];
    for (int i = 1; i < length; i++)
    {
        if (Max < array[i])
        {
            Max = array[i];
        }
    }

    return Max;
}

int main()
{
    srand(time(NULL));
    int M, N, K;

    int Choose = -1;

    printf("---------------------------------------\n");
    printf("| Choose (1) to choose elements       |\n");
    printf("| Choose (2) to choose control ex.    |\n");
    printf("---------------------------------------\n");
    scanf("%i", &Choose);

    if (Choose == 1)
    {
        bool True = false;
        while (!True)
        {
            printf("Enter N: ");
            scanf("%i", &N);
            printf("Enter M: ");
            scanf("%i", &M);
            printf("Enter K: ");
            scanf("%i", &K);

            if (M > 0 && N > 0)
            {
                True = true;
            }
        }
    }
    else if (Choose == 2)
    {
        M = 8;
        N = 8;
        K = 6;
    }

    int matrix[M][N];

    for (int j = 0; j < M; j++)
    {
        for (int i = 0; i < N; i++)
        {
            matrix[j][i] = rand() % 90 + 10;
        }
    }

    printf(KBLU "Matrix BEFORE\n" RESET);
    for (int j = 0; j < M; j++)
    {
        for (int i = 0; i < N; i++)
        {
            if (matrix[j][i] % K == 0 || !Parity(matrix[j][i] % 2))
            {
                printf(KRED "%i  " RESET, matrix[j][i]);
            }
            else
            {
                printf("%i  ", matrix[j][i]);
            }
        }
        printf("\n");
    }

    int array[M * N];
    int length = 0;

    for (int j = 0; j < M; j++)
    {
        for (int i = 0; i < N; i++)
        {
            if (matrix[j][i] % K == 0 || !Parity(matrix[j][i] % 2))
            {
                array[length] = matrix[j][i];
                length++;
            }
        }
    }

    int Max = FindMax(length, array);

    int count[Max + 1];
    for (int i = 0; i < Max + 1; i++)
    {
        count[i] = 0;
    }

    for (int i = 0; i < length; i++)
    {
        count[array[i]]++;
    }

    for (int i = 1; i < Max + 1; i++)
    {
        count[i] += count[i - 1];
    }

    int sorted[length];
    for (int i = 0; i < length; i++)
    {
        sorted[i] = 0;
    }

    for (int i = length - 1; i >= 0; i--)
    {
        sorted[count[array[i]] - 1] = array[i];
        count[array[i]]--;
    }
    
    int t = 0;
    for (int j = 0; j < M; j++)
    {
        for (int i = 0; i < N; i++)
        {
            if (matrix[j][i] % K == 0 || !Parity(matrix[j][i] % 2))
            {
                matrix[j][i] = sorted[t];
                t++;
            }
        }
    }

    printf("\n\n");

    printf(KBLU "Matrix AFTER\n" RESET);
    for (int j = 0; j < M; j++)
    {
        for (int i = 0; i < N; i++)
        {
            if (matrix[j][i] % K == 0 || !Parity(matrix[j][i] % 2))
            {
                printf(KGRN "%i  " RESET, matrix[j][i]);
            }
            else
            {
                printf("%i  ", matrix[j][i]);
            }
        }
        printf("\n");
    }
}