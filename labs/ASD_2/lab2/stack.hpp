#pragma once
#include "dyn_array.hpp"
#include <string>
#include <iostream>
#include <fstream>
#include <cstdlib>
using namespace std;

template <typename T>
class Stack
{
    DynArray<T> stack_;
    int length_;

public:
    Stack()
    {
        length_ = 0;
        DynArray<T> stack_;
    }

    int size_stack()
    {
        return length_;
    }

    T pop()
    {
        if(!empty())
        {
            T tmp = stack_[size_stack() - 1];
            length_--;
            return tmp;
        }
        else
        {
            cerr << "You try pop empty stack" << endl;
            exit(1);
        }
        
    }

    void push(T value)
    {
        if(length_ + 1 == stack_.capacity())
        {
            stack_.resize(2 * stack_.capacity());
        }
        length_++;
        stack_[length_ - 1] = value;
    }

    T top()
    {
        if(!empty())
            return stack_[size_stack() - 1];
        else
        {
            cerr << "Stack is epmty " << endl;
            exit(1);
        }
    }

    bool empty()
    {
        return !size_stack();
    }

    void print()
    {
        cout << "--------" << endl;
        for (int i = 0; i < length_; i++)
        {
            cout << "|" << stack_[i] << endl;
        }
        cout << "--------" << endl;
    }
};