#include "stack.hpp"
#include <iostream>
#include <string>
#include <cctype>
using namespace std;
#define KNRM "\x1B[0m"
#define KRED "\x1B[31m"
#define KGRN "\x1B[32m"
#define KYEL "\x1B[33m"
#define KBLU "\x1B[34m"
#define KMAG "\x1B[35m"
#define KCYN "\x1B[36m"
#define KWHT "\x1B[37m"
#define RESET "\x1B[0m"

bool verification_string(std::string equation)
{
    bool exist = false;
    for(int i = 0; equation[i] != '\0'; i++)
    {
        if(equation[i] == '+' || equation[i] == '-' || equation[i] == '*' || equation[i] == '/')
        {
            if(!exist)
            {
                exist = true;
            }
            else
            {
                return false;
            }
        }
        else if (equation[i] == ' ')
        {
        }
        else
        {
            exist = false;
        }
        
    }

    return true;
}

bool valid_braces(std::string braces)
{
    Stack<char> br_count;
    char j;
    for (int i = 0; braces[i] != '\0'; i++)
    {
        j = ' ';
        if (braces[i] == '(')
            br_count.push('(');
        if (braces[i] == ')')
        {
            if (br_count.top() == '(')
                br_count.pop();
            else
                return false;
        }
    }

    return br_count.empty();
};



int getPriority(char sym)
{
    if (sym == '*' || sym == '/')
    {
        return 3;
    }
    else if (sym == '+' || sym == '-')
    {
        return 2;
    }
    else if (sym == ')')
    {
        return 1;
    }
    else if (sym == '(')
    {
        return 0;
    }
    else
    {
        return -1;
    }
}

int main()
{
    bool exit_program = false;
    int choose;
    while (!exit_program)
    {
        cout << "-------------------" << endl
             << "|Choose action:   |" << endl
             << "|1) Control       |" << endl
             << "|2) Your own      |" << endl
             << "|3) Exit          |" << endl
             << "-------------------" << endl;
        cin >> choose;
        if (choose == 1 || choose == 2)
        {
            string equation("");
            if (choose == 1)
            {
                equation = "( 10 + 190 ) / ( 5 + 6 )";
                cout << "Control expression is : '" << equation << "'" << endl;
            }
            else
            {
                cout << "Enter your own expression (with whitespace) : " << endl;
                getchar();
                std::getline(std::cin, equation);
            }
            if (valid_braces(equation) && verification_string(equation))
            {
                cout << endl
                     << KYEL << "Expression is : '" << equation << "'" << RESET << endl;
                string answer("");
                Stack<char> signs;
                for (int i = 0; i < equation.length(); i++)
                {
                    if (isdigit(equation[i]))
                    {
                        while (i < equation.length() && (isdigit(equation[i]) || equation[i] == '.'))
                        {
                            answer += equation[i];
                            i++;
                        }
                        answer += ' ';
                    }
                    else if (equation[i] == ' ')
                    {
                    }
                    else
                    {
                        if (signs.empty())
                        {
                            signs.push(equation[i]);
                        }
                        else if (equation[i] == '(')
                        {
                            signs.push(equation[i]);
                        }
                        else if (equation[i] == ')')
                        {
                            while (signs.top() != '(')
                            {
                                if (!signs.empty())
                                {
                                    answer += signs.pop();
                                    answer += ' ';
                                }
                                else
                                {
                                    cerr << "ERROR" << endl;
                                }
                            }
                            signs.pop();
                        }
                        else
                        {
                            while (!signs.empty() && getPriority(equation[i]) <= getPriority(signs.top()))
                            {
                                answer += signs.pop();
                                answer += ' ';
                            }
                            signs.push(equation[i]);
                        }
                    }
                }
                while (!signs.empty())
                {
                    answer += signs.pop();
                    answer += ' ';
                }
                cout << KGRN << "Postfix notation is : '" << answer << "'" << RESET << endl;

                Stack<float> num;
                string tmp_str("");
                float tmp1 = 0;
                float tmp2 = 0;
                for (int i = 0; i < answer.length(); i++)
                {
                    tmp_str = "";
                    if (isdigit(answer[i]))
                    {
                        while (i < answer.length() && (isdigit(answer[i]) || answer[i] == '.'))
                        {
                            tmp_str += answer[i];
                            i++;
                        }
                        num.push(stof(tmp_str));
                    }
                    else
                    {
                        if (answer[i] == '+')
                        {
                            tmp1 = num.pop();
                            tmp2 = num.pop();
                            num.push(tmp1 + tmp2);
                        }
                        if (answer[i] == '-')
                        {
                            tmp1 = num.pop();
                            tmp2 = num.pop();
                            num.push(tmp2 - tmp1);
                        }
                        if (answer[i] == '*')
                        {
                            tmp1 = num.pop();
                            tmp2 = num.pop();
                            num.push(tmp2 * tmp1);
                        }
                        if (answer[i] == '/')
                        {
                            tmp1 = num.pop();
                            tmp2 = num.pop();
                            num.push(tmp2 / tmp1);
                        }
                    }
                }
                float res = num.pop();
                cout << KRED << "Result is : " << res << RESET << endl
                     << endl;
            }
            else
            {
                cerr << KRED << "Equation is not is incorrect" << RESET << endl;
                exit(1);
            }
        }
        else if (choose == 3)
        {
            exit_program = true;
        }
        else
        {
            cerr << "Choose other number" << endl;
            exit(1);
        }
    }
}