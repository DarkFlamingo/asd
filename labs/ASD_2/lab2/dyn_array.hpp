#pragma once
#include <string>
#include <iostream>
#include <fstream>
#include <cstdlib>
using namespace std;

template <typename T>
class DynArray
{
    T *items_;
    int capacity_;

public:
    DynArray()
    {
        capacity_ = 16;
        T *tmp = new T[capacity_];
        if (tmp == nullptr)
        {
            cerr << "ERROR."
                 << "Memory" << endl;
            exit(1);
        }
        items_ = tmp;
    }

    ~DynArray()
    {
        delete[] items_;
        capacity_ = 0;
    }

    int capacity()
    {
        return capacity_;
    }

    T &operator[](int index)
    {
        if (index >= 0 && index < capacity_)
            return items_[index];
        else
        {
            cerr << "ERROR."
                 << "Incorrect index" << endl;
            exit(1);
        }
    }

    void resize(int newSize)
    {
        T *tmp = new T[newSize];
        if (tmp == nullptr)
        {
            cerr << "ERROR."
                 << "Memory" << endl;
            exit(1);
        }
        for (int i = 0; i < capacity_; i++)
        {
            tmp[i] = items_[i];
        }
        T *t = items_;
        delete[] t;
        items_ = tmp;
        capacity_ = newSize;
    }
};