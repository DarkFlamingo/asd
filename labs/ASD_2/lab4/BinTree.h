#pragma once
#include "Node.h"

#include <string>

using namespace std;

class BinTree
{
    Node * root = nullptr;
    Node * this_Node;
    int size = 0;

public:

    BinTree();
    int sizeOfTree();
    void insert(string data_);
    void deleteTree();
    void print();
    bool calculateExp();
};