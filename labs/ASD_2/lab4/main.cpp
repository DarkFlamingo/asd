#include "parseString.h"
#include "BinTree.h"
#include <stack>

using namespace std;

bool goodString(vector<string> exp)
{
    int colOper = 0;
    int colPar = 0;
    std::stack<int> tmp;
    for(int i = 0; i < exp.size(); i++)
    {
        if(exp[i][0] == '&' || exp[i][0] == '|' || exp[i][0] == '!')
        {
            colOper++;
        }
        if(exp[i][0] == '(')
        {
            colPar++;
            tmp.push(1);
        }
        if(exp[i][0] == ')')
        {
            tmp.pop();
        }
    }

    if(!tmp.empty())
    {
        return false;
    }
    if(colOper != colPar)
    {
        return false;
    }

    return true;
}

int main()
{
    int ch;
    cout << "Your string or ex" << endl;
    cout << "(1) - own" << endl;
    cout << "(2) - ex" << endl;
    cin >> ch;
    string tmp;
    if(ch == 1)
    {
        cout << "Enter your string: " << endl;
        cin >> tmp;
    }
    else
    {
        tmp = "((T&&F)||(!(T&&F)))";
    }
    cout << "Expresion is: " << tmp << endl;
    vector<string> array = parceStringIntoVector(tmp);
    if(goodString(array))
    {
        cout << endl << endl;
        BinTree Tree;
        for(int i = 0; i < array.size(); i++)
        {
            Tree.insert(array[i]);
        }
        Tree.print();
        cout << endl << endl;
        bool res = Tree.calculateExp();
        if(res)
        {
            cout << "TRUE" << endl;
        }
        else
        {
            cout << "FALSE" << endl;
        }
        Tree.deleteTree();
        }   
    else
    {
        cerr << "ERROR Incorrect exp" << endl;
        exit(1);
    }
}