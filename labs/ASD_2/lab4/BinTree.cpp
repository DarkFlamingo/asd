#include "BinTree.h"
#include "myLogicalOperations.h"

#include <vector>
#include <string>

int BinTree::sizeOfTree()
{
    return this->size;
}

BinTree::BinTree()
{
    if(root == nullptr)
    {
        Node * tmp = new Node;
        if(tmp != nullptr)
        {
            root = tmp;
            root->data = "";                
            root->left = nullptr;
            root->right = nullptr;
            root->parent = nullptr; 
            this_Node = root;
        }
        else
        {
            cout << "Member ERROR <constructor>" << endl;
            exit(1);
        }
    }
}

void BinTree::insert(string data_)
{
        if(data_ == "(")
        {
            Node *tmp = new Node;
            if(tmp != nullptr)
            {
                this_Node->left = tmp;
                this_Node->left->left = nullptr;
                this_Node->left->right = nullptr;
                this_Node->left->parent = this_Node;
                this_Node = this_Node->left;
            }
            else
            {
                cout << "Member ERROR <insert>" << endl;
                exit(1);
            }
        }
        else if(data_ == "&&" || data_ == "||")
        {
            this_Node->data = data_;
            Node *tmp = new Node;
            if(tmp != nullptr)
            {
                this_Node->right = tmp;
                this_Node->right->left = nullptr;
                this_Node->right->right = nullptr;
                this_Node->right->parent = this_Node;
                this_Node = this_Node->right;
            }
            else
            {
                cout << "Member ERROR <insert>" << endl;
                exit(1);
            }
        }
        else if(data_ == "!")
        {
            this_Node->parent->data = data_;
        }
        else if(data_ == "T" || data_ == "F")
        {
            this_Node->data = data_;
            this_Node = this_Node->parent;   
        }
        else if(data_ == ")")
        {
            this_Node = this_Node->parent;
        }
}

void clear(Node *& tree)
{
    if (tree->left)
    {
        clear(tree->left);
    }
    if (tree->right)
    {
        clear(tree->right);
    }
    delete tree;
}

void BinTree::deleteTree()
{
    clear(this->root);
}

void printValueOnLevel(Node *node, char pos, int depth)
{
    for (int i = 0; i < depth; i++)
    {
        printf("....");
    }
    printf("%c: ", pos);

    if (node == nullptr)
    {
        printf("(null)\n");
    }
    else
    {
        cout << node->data << endl;
    }
}

void printNode(Node *node, char pos, int depth)
{
    bool hasChild = node != nullptr && (node->left != nullptr || node->right != nullptr);
    if (hasChild)
        printNode(node->right, 'R', depth + 1);
    printValueOnLevel(node, pos, depth);
    if (hasChild)
        printNode(node->left, 'L', depth + 1);
}

void printBinTree(Node *root)
{
    printNode(root, '+', 0);
}

void BinTree::print()
{
    printBinTree(root);
}

void passage(Node * tree)
{
    if(tree != nullptr)
    {
        passage(tree->left);
        passage(tree->right);
        cout << tree->data;//do something
    }
}

string evaluate(Node * root)
{
    Node *leftC = root->left;
    Node *rightC = root->right;

    if(leftC != nullptr && rightC != nullptr)
    {
        return myLogicalAnswer(evaluate(root->left), evaluate(root->right), root->data);
    }
    else if(leftC != nullptr)
    {
        string tmp = "";
        return myLogicalAnswer(evaluate(root->left), tmp, root->data);
    }
    else
    {
        return root->data;
    }
}

bool BinTree::calculateExp()
{
    string res = evaluate(this->root);
    if(res == "T")
    {
        return true;
    }
    else if(res == "F")
    {
        return false;
    }
    else
    {
        cerr << "ERROR <calculateExp>" << endl;
        exit(1);
    }
}