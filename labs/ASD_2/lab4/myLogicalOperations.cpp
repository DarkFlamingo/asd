#include "myLogicalOperations.h"
#include <iostream>

string mylogicalNo(string one)
{
    if(one == "T")
    {
        return "F";
    }
    else if(one == "F")
    {
        return "T";
    }
    else
    {
        cerr << "ERROR <logicalNo>" << endl;
        exit(1);   
    }
}

string mylogicalOr(string one, string other)
{
    if(one == "T" || other == "T")
    {
        return "T";
    }
    else if(one == "F" || other == "F")
    {
        return "F";
    }
    else
    {
        cerr << "ERROR <logicalOr>" << endl;
        exit(1);   
    }
}

string mylogicalAnd(string one, string other)
{
    if(one == "T" && other == "T")
    {
        return "T";
    }
    else if(one == "F" || other == "F")
    {
        return "F";
    }
    else
    {
        cerr << "ERROR <logicalAnd>" << endl;
        exit(1);   
    }
}

string myLogicalAnswer(string one, string other, string str_operator)
{
    if(str_operator == "||")
    {
        return mylogicalOr(one, other); 
    }
    else if(str_operator == "&&")
    {
        return mylogicalAnd(one, other);
    }
    else if(str_operator == "!")
    {
        if(one.size() != 0 || other.size() != 0)
        {
            if(one.size() != 0)
            {
                return mylogicalNo(one);
            }
            else
            {
                return mylogicalNo(other);   
            }
        }
        else
        {
            cerr << "ERROR <logicalAnswer> operand" << endl;
            exit(1);   
        }
    }
    else
    {
        cerr << "ERROR <logicalAnswer>" << endl;
        exit(1);
    }
}