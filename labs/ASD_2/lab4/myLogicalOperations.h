#pragma once
#include <string>

using namespace std;

string myLogicalNo(string one);
string myLogicalOr(string one, string other);
string myLogicalAnd(string one, string other);
string myLogicalAnswer(string one, string other, string str_operator);