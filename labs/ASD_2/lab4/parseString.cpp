#include "parseString.h"

vector<string> parceStringIntoVector(string exp)
{
    vector<string> result;
    int k = 0;
    for(int i = 0; exp[i] != '\0'; i++)
    {
        if(exp[i] == '(' && exp[i] == ')')
        {
            result.push_back("");
            result[k].push_back(exp[i]);
            k++;
        }
        else if(exp[i] == '&')
        {
            if(i != 0)
            {
                if(exp[i - 1] == '&')
                {
                    result.push_back("");
                    result[k].push_back(exp[i - 1]);
                    result[k].push_back(exp[i]);
                    k++;
                }
            }
            else
            {
                cout << "ERROR parsing. Wrong expression";
                exit(1);
            }
        }
        else if(exp[i] == '|')
        {
            if(i != 0)
            {
                if(exp[i - 1] == '|')
                {
                    result.push_back("");
                    result[k].push_back(exp[i - 1]);
                    result[k].push_back(exp[i]);
                    k++;
                }
            }
            else
            {
                cout << "ERROR parsing. Wrong expression";
                exit(1);
            }
        }
        else if(exp[i] == ' ')
        {
            
        }
        else
        {
            result.push_back("");
            result[k].push_back(exp[i]);
            k++;
        }
    }

    int sizeArray = 0;
    for(int i = 0; i < result.size(); i++)
    {
        sizeArray += result[i].size();
    }

    if(sizeArray != exp.size())
    {
        cerr << "ERROR incorrect exp" << endl;
        exit(1);
    }
    
    return result;
}