#include "node.h"

void copyNode(Node &first, Node &second)
{
    first.deleted = second.deleted;
    first.exist = second.exist;
    first.key.username = second.key.username;
    first.value.emailAddress = second.value.emailAddress;
    first.value.password = second.value.password;
}

void writeNode(Node &first, HashtableKey key_, HashtableValue value_)
{
    first.deleted = false;
    first.exist = false;
    first.key.username = key_.username;
    first.value.emailAddress = value_.emailAddress;
    first.value.password = value_.password;
}