#include <string>
#include <vector>
#include <iostream>
#include <cmath>
#include "hash_table.h"
#include "addition.h"

using namespace std;

void HashTable::printInfo()
{
    cout << "size is :" << this->size << endl;
    cout << "capacity is :" << this->capacity << endl;
    cout << "loadness is :" << this->loadness << endl;
}

bool HashTable::isEmpty()
{
    return !(static_cast<bool>(size));
}

void HashTable::initHashtable()
{
    int HASHTABLE_CAPACITY = 89;
    capacity = HASHTABLE_CAPACITY;
    Node *tmp = new Node[capacity];
    if (tmp == nullptr)
    {
        cerr << "ERROR."
             << "Memory" << endl;
        exit(1);
    }
    table = tmp;
    size = 0;
    this->loadness = (static_cast<float>(size))/ (static_cast<float>(capacity));
    for (int i = 0; i < capacity; i++)
    {
        table[i].exist = false;
        table[i].deleted = false;
    }
}

HashTable::HashTable()
{
    initHashtable();
}

HashTable::~HashTable()
{
    size = 0;
    capacity = 0;
    loadness = 0;
    delete[] table;
}

void HashTable::printHashTable()
{
    cout << "Hash Table :" << endl;
    cout << "-------------------------------" << endl;
    for (int i = 0; i < capacity; i++)
    {
        if (table[i].exist)
        {
            cout << '[' << i << ']' << " - " << table[i].key.username << " || " << table[i].value.emailAddress << " || " << table[i].value.password << endl;
        }
    }
    cout << "-------------------------------" << endl;
}

int HashTable::hashCode(HashtableKey key)
{
    int res = 0;
    for (int i = 0; key.username[i] != '\0'; i++)
    {
        if (isdigit(key.username[i]))
        {
            string tmp;
            tmp.push_back(key.username[i]);
            int t = stoi(tmp);
            if (t == 0)
            {
                t = 238;
            }

            res += t * (27 ^ i);
        }
        if (isalpha(key.username[i]))
        {
            res += key.username[i] * (27 ^ i);
        }
    }
    return res;
}

int HashTable::getHash(HashtableKey key)
{
    int hash_key = hashCode(key);
    return hash_key % this->capacity;
}

int HashTable::doesUsernameExit(string username_)
{
    if (!isEmpty())
    {
        HashtableKey key_;
        key_.username = username_;
        bool found = false;
        bool check = false;
        int pos = -1;
        int hash_ = getHash(key_);
        while (!found)
        {
            if (table[hash_].key.username == username_ && !table[hash_].deleted)
            {
                pos = hash_;
                found = true;
            }
            else if (!table[hash_].exist && !table[hash_].deleted)
            {
                pos = -1;
                found = true;
            }
            else
            {
                hash_ = ((hashCode(key_) + static_cast<int>(pow(hash_, 2))) % capacity);
            }
        }
        return pos;
    }
    else
    {
        return -1;
    }
}

void HashTable::doesUserExist(string username_, string password_)
{
    int hash_key = doesUsernameExit(username_);
    if (hash_key != -1)
    {
        if (table[hash_key].value.password == std::to_string(getHashPassword(password_)))
        {
            HashtableKey key_;
            key_.username = username_;
            cout << "User found : " << '[' << hash_key << ']' << " - " << table[hash_key].key.username << " || " << table[hash_key].value.emailAddress << " || " << table[hash_key].value.password << endl;
        }
        else
        {
            cout << "Password incorrert !" << endl;
        }
    }
    else
    {
        cout << "User " << username_ << " not found" << endl;
    }
}

void HashTable::insertEntry(HashtableKey key, HashtableValue value)
{
    bool inserted = false;
    int hash_index = getHash(key);
    Node tmp;
    writeNode(tmp, key, value);
    tmp.value.password = std::to_string(getHashPassword(tmp.value.password));
    tmp.exist = true;
    if (loadness < 0.5)
    {
        int index_found = doesUsernameExit(key.username);
        if (index_found == -1)
        {
            while (!inserted)
            {
                if (!table[hash_index].exist && !table[hash_index].deleted)
                {
                    copyNode(table[hash_index], tmp);
                    inserted = true;
                    size++;
                    this->loadness = (static_cast<float>(size))/ (static_cast<float>(capacity));
                }
                else
                {
                    hash_index = ((hashCode(key) + static_cast<int>(pow(hash_index, 2))) % capacity);
                }
            }
        }
        else
        {
            copyNode(table[index_found], tmp);
        }
    }
    else
    {
        cout << "Rehashing" << endl;
        rehashing();
        insertEntry(key, value);
    }
}

void HashTable::insertEntryReahashing(HashtableKey key, HashtableValue value)
{
    bool inserted = false;
    int hash_index = getHash(key);
    Node tmp;
    writeNode(tmp, key, value);
    tmp.exist = true;
    if (loadness < 0.5)
    {
        int index_found = doesUsernameExit(key.username);
        if (index_found == -1)
        {
            while (!inserted)
            {
                if (!table[hash_index].exist && !table[hash_index].deleted)
                {
                    copyNode(table[hash_index], tmp);
                    inserted = true;
                    size++;
                    this->loadness = (static_cast<float>(size))/ (static_cast<float>(capacity));
                }
                else
                {
                    hash_index = ((hashCode(key) + static_cast<int>(pow(hash_index, 2))) % capacity);
                }
            }
        }
        else
        {
            copyNode(table[index_found], tmp);
        }
    }
    else
    {
        cout << "Rehashing" << endl;
        rehashing();
        insertEntryReahashing(key, value);
    }
}

bool HashTable::removeEntry(HashtableKey key)
{
    int index_found = doesUsernameExit(key.username);
    if (index_found != -1)
    {
        table[index_found].exist = false;
        table[index_found].deleted = true;
        size--;
        return true;
    }
    else
    {
        return false;
    }
}

bool isSimple(int num)
{
    int i;
    bool isPrime = true;
    for (i = 2; i <= (sqrt(abs(num))); i++)
    {
        if (num % i == 0)
        {
            isPrime = false;
            break;
        }
    }
    if (isPrime)
        return true;
    else
        return false;
}

int simpleNumber(int num)
{
    while (!isSimple(num))
    {
        num++;
    }
    return num;
}

void HashTable::rehashing()
{
    int new_capacity = simpleNumber(capacity * 2);
    cout << "Simple number is : " << new_capacity << endl;
    int old_capacity = capacity;
    this->capacity = new_capacity;
    this->loadness = (static_cast<float>(size))/ (static_cast<float>(capacity));;
    Node *tmp = new Node[capacity];
    if (tmp == nullptr)
    {
        cerr << "ERROR."
             << "Memory" << endl;
        exit(1);
    }
    vector<Node> array;
    for (int i = 0; i < old_capacity; i++)
    {
        if (table[i].exist)
        {
            HashtableKey key_;
            key_.username = table[i].key.username;
            HashtableValue value_;
            value_.emailAddress = table[i].value.emailAddress;
            value_.password = table[i].value.password;
            Node tmp;
            tmp.deleted = false;
            tmp.exist = true;
            writeNode(tmp, key_, value_);
            array.push_back(tmp);
        }
    }
    delete[] table;
    table = tmp;
    for (int i = 0; i < array.size(); i++)
    {
        insertEntryReahashing(array[i].key, array[i].value);
    }
}