#include "value.h"
#include "key.h"

struct Node
{
    HashtableKey key;
    HashtableValue value;
    bool deleted = false;
    bool exist = false;
};

void copyNode(Node &first, Node &second);
void writeNode(Node &first, HashtableKey key_, HashtableValue value_);