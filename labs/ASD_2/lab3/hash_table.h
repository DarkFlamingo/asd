#pragma once

#include <vector>

#include "node.h"

using namespace std;

class HashTable
{
    Node *table;
    float loadness;
    int size;
    int capacity;

public:
    HashTable();
    ~HashTable();
    void initHashtable();
    void insertEntry(HashtableKey key, HashtableValue value);
    bool removeEntry(HashtableKey key);
    void rehashing();
    int hashCode(HashtableKey key);
    int getHash(HashtableKey key);

    void doesUserExist(string username, string password);
    int doesUsernameExit(string username);
    //addition
    bool isEmpty();
    void printHashTable();
    void printInfo();
    void insertEntryReahashing(HashtableKey key, HashtableValue value);
};