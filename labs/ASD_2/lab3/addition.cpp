#include "addition.h"

int getHashPassword(string Password)
{
    int res = 0;
    for (int i = 0; Password[i] != '\0'; i++)
    {
        if (isdigit(Password[i]))
        {
            string tmp;
            tmp.push_back(Password[i]);
            int t = stoi(tmp);
            if (t == 0)
            {
                t = 238;
            }

            res += t * (27 ^ i);
        }
        if (isalpha(Password[i]))
        {
            res += Password[i] * (27 ^ i);
        }
    }
    return res;
}