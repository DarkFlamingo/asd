#pragma once
#include "hash_table.h"

class Cui
{

    void mainMenu(HashTable &table);
    void loginMenu(HashTable &table);
    void registrationMenu(HashTable &table);
    void deleteMenu(HashTable &table);

public:
    void show();
};