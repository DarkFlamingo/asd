#include "cui.h"
#include "hash_table.h"
#include <iostream>
#include "addition.h"
#include <limits>

using namespace std;

void Cui::loginMenu(HashTable &table)
{
    string username_entr;
    string password_entr;
    cout << endl
         << endl
         << "     Username : ";
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
    getline(std::cin, username_entr);
    cout << endl
         << "     Password : ";
    getline(std::cin, password_entr);
    cout << endl
         << endl;
    table.doesUserExist(username_entr, password_entr);
}

void Cui::registrationMenu(HashTable &table)
{
    string username_entr;
    string password_entr;
    string username_email;
    cout << endl
         << endl
         << "     Username : ";
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
    getline(std::cin, username_entr);
    cout << endl
         << "     Password : ";
    getline(std::cin, password_entr);
    cout << endl
         << "     Email    : ";
    getline(std::cin, username_email);
    cout << endl
         << endl;
    HashtableKey key;
    key.username = username_entr;
    HashtableValue value;
    value.emailAddress = username_email;
    value.password = password_entr;
    table.insertEntry(key, value);
}

void Cui::deleteMenu(HashTable &table)
{
    string username_entr;
    cout << endl
         << endl
         << "     Username : ";
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
    getline(std::cin, username_entr);
    HashtableKey key_;
    key_.username = username_entr;
    if(table.removeEntry(key_))
    {
        cout << "User was delete" << endl;
    }
    else
    {
        cout << "User was not delete. Not found or not exist" << endl;
    }
}

void Cui::mainMenu(HashTable &table)
{
    int i = -1;
    while (1)
    {
        cout << "What to want to do " << endl
             << "(1) Reg..." << endl
             << "(2) Login..." << endl
             << "(3) Delete... " << endl
             << "(4) Show all... " << endl
             << "(0) Exit " << endl;
        cin >> i;
        if (i == 1)
        {
            registrationMenu(table);
            table.printHashTable();
        }
        else if (i == 2)
        {
            loginMenu(table);
            table.printHashTable();
        }
        else if (i == 3)
        {
            deleteMenu(table);
            table.printHashTable();
        }
        else if (i == 4)
        {
            table.printHashTable();
        }
        else if (i == 0)
        {
            break;
        }
        cout << endl;
    }
}

void Cui::show()
{
    cout << "Choose new hash table ?" << endl
         << "(1) Yes " << endl
         << "(2) No " << endl
         << "(0) Exit " << endl
         << endl;
    int choose = -1;
    HashTable table;
    table.initHashtable();
    cin >> choose;
    if (choose == 1)
    {
    }
    else if (choose == 2)
    {
        HashtableKey key;
        key.username = "max";
        HashtableValue value;
        value.emailAddress = "max@gmail.com";
        value.password = "1235344";
        table.insertEntry(key, value);

        key.username = "den";
        value.emailAddress = "den@gmail.com";
        value.password = "123676544";
        table.insertEntry(key, value);   

        key.username = "slavik";
        value.emailAddress = "slavik@gmail.com";
        value.password = "123534264";
        table.insertEntry(key, value);   

        key.username = "maria";
        value.emailAddress = "maria@gmail.com";
        value.password = "123532577754";
        table.insertEntry(key, value);   

        key.username = "sasha";
        value.emailAddress = "sasha@gmail.com";
        value.password = "12363634534";
        table.insertEntry(key, value);   

        key.username = "tolia";
        value.emailAddress = "tolia@gmail.com";
        value.password = "123647657564";
        table.insertEntry(key, value);   
    }
    else if (choose == 0)
    {
        return;
    }
    mainMenu(table);
}