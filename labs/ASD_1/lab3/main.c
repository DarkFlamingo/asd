#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#define KRED "\x1B[31m"
#define RESET "\x1B[0m"

bool Par(int j)
{
    float prov = j / 2.0;
    if (prov - (int)prov == 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

int main()
{
    srand(time(0));

    int N;
    int M;

    printf("Enter M : ");
    scanf("%i", &M);
    printf("Enter N : ");
    scanf("%i", &N);

    if ((N > 0) && (M > 0))
    {
        int arr[M][N];
        int K;
        printf("Enter K : ");
        scanf("%i", &K);
        int L;
        printf("Enter L : ");
        scanf("%i", &L);

        bool T;

        for (int j = 0; j < M; j++)
        {
            for (int i = 0; i < N;)
            {
                T = false;
                int Random = rand() % (N * M * 2 - 0 + 1) + 0;

                for (int k = 0; k < M; k++)
                {
                    for (int u = 0; u < N; u++)
                    {
                        if (Random == arr[k][u])
                        {
                            T = true;
                        }
                    }
                }

                if (T == false)
                {
                    arr[j][i] = Random;
                    i++;
                }
            }
        }

        for (int j = 0; j < M; j++)
        {
            for (int i = 0; i < N; i++)
            {
                if (((int)((arr[j][i]) / K) < L) && (!Par(i)))
                {
                    if (arr[j][i] < 10)
                    {
                        printf(KRED "%i   " RESET, arr[j][i]);
                    }
                    else
                    {
                        printf(KRED "%i  " RESET, arr[j][i]);
                    }
                }
                else
                {
                    if (arr[j][i] < 10)
                    {
                        printf("%i   ", arr[j][i]);
                    }
                    else
                    {
                        printf("%i  ", arr[j][i]);
                    }
                }
            }
            printf("\n");
        }

        if (N > 1)
        {

            int buf[M * N];
            int h = 0;

            for (int i = 0; i < M * N; i++)
            {
                buf[i] = 0;
            }

            for (int i = 1; i < N; i = i + 2)
            {
                for (int j = 0; j < M; j++)
                {
                    if ((int)((arr[j][i]) / K) < L)
                    {
                        buf[h] = arr[j][i];
                        h++;
                    }
                }
            }

            bool sorted = false;

            int start = 0;
            int end = h - 1;

            while (!sorted)
            {
                sorted = true;

                for (int i = start; i < end; i++)
                {
                    if (buf[i] < buf[i + 1])
                    {
                        int t = buf[i];
                        buf[i] = buf[i + 1];
                        buf[i + 1] = t;
                        sorted = false;
                    }
                }

                if (sorted)
                {
                    break;
                }

                sorted = true;

                end--;

                for (int i = end - 1; i >= start; i--)
                {
                    if (buf[i] < buf[i + 1])
                    {
                        int t = buf[i];
                        buf[i] = buf[i + 1];
                        buf[i + 1] = t;
                        sorted = false;
                    }
                }

                start++;
            }

            int o = 0;

            for (int i = 1; i < N; i = i + 2)
            {
                for (int j = 0; j < M; j++)
                {
                    if ((int)((arr[j][i]) / K) < L)
                    {
                        arr[j][i] = buf[o];
                        o++;
                    }
                }
            }

            printf("\n");

            for (int j = 0; j < M; j++)
            {
                for (int i = 0; i < N; i++)
                {
                    if (((int)((arr[j][i]) / K) < L) && (!Par(i)))
                    {
                        if (arr[j][i] < 10)
                        {
                            printf(KRED "%i   " RESET, arr[j][i]);
                        }
                        else
                        {
                            printf(KRED "%i  " RESET, arr[j][i]);
                        }
                    }
                    else
                    {
                        if (arr[j][i] < 10)
                        {
                            printf("%i   ", arr[j][i]);
                        }
                        else
                        {
                            printf("%i  ", arr[j][i]);
                        }
                    }
                }
                printf("\n");
            }
        }
        else
        {
            printf("Nothing to sort\n");
        }
    }
}