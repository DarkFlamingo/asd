#include <math.h>
#include <stdio.h>
#include <stdbool.h>

int rang(long long int x)
{
    long long int t = x;
    int c = 1;

    while (t >= 10)
    {
        t = x / pow(10,c);

        c = c + 1;
    }

    return c;
}

int main()
{
    long long int num, i, j, f, e, R = 0;

    printf("Enter your number: ");
    scanf("%lld", &num);

    if (num <= 0)
    {
        printf("ERROR\n");
    }
    else 
    {
        for(long long int k = 1;k <= num; k++)
        {
            j = rang(k);

            f = pow(k,2);

            R = pow(10,j);

            e = f % R;

            if(e == k)
            {
                printf("Num is: %lld\n", k);
            }
        }
    }
}