#include <math.h>
#include <stdio.h>
#include <stdbool.h>

int main()
{
    float a, b, x, y, z, p, w, q= 0;

    printf("Enter x: ");
    scanf("%f", &x);

    printf("Enter y: ");
    scanf("%f", &y);

    printf("Enter z: ");
    scanf("%f", &z);

    p = fabs((-x - 2));
    q = fabs((int)p);
    w = p - q;

    if(((pow(x, 2) + 4) == 0) || ((pow(x, 2) - 4) == 0) || (((pow(z, (-x-2))) + (1 / (pow(x, 2) - 4))) == 0) || (pow(x, 4)/2 - (pow(sin(z), 2)) == 0) || ((z < 0) && (w != 0)) || ((z == 0) && ((-x - 2) <= 0)))
    {
        printf("ERROR.\n");
    }
    else
    {
        a = (1 + y)*((x + (y/(pow(x, 2) + 4))) / ((pow(z, -x-2)) + 1/(pow(x, 2) - 4)));
        printf("a = %f\n", a);

        b = b = (1 + (pow(cos(a- 2), y))) / (((pow(x, 4)) / 2) - (pow(sin(z), 2)));
        printf("b = %f\n", b);
    }
}