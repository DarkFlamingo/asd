#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <stdbool.h>
#include <math.h>
#include <progbase.h>
#include <progbase/console.h>
#include <progbase/canvas.h>
#define KNRM "\x1B[0m"
#define KRED "\x1B[31m"
#define KGRN "\x1B[32m"
#define KYEL "\x1B[33m"
#define KBLU "\x1B[34m"
#define KMAG "\x1B[35m"
#define KCYN "\x1B[36m"
#define KWHT "\x1B[37m"
#define RESET "\x1B[0m"

struct SLNode
{
    int data;
    struct SLNode *next;
};

struct SLNode *createSLNode(int data)
{
    struct SLNode *node = malloc(sizeof(struct SLNode));
    node->data = data;
    node->next = NULL;
    return node;
}

int sizeSL(struct SLNode *list)
{
    int length = 0;
    while (list != NULL)
    {
        length++;
        list = list->next;
    }
    return length;
}

void printSLList(struct SLNode *list) //head
{
    printf("List :");
    size_t length = sizeSL(list);
    while (list != NULL)
    {
        if (list->next != NULL)
        {
            printf("%i->", list->data);
        }
        else
        {
            printf("%i", list->data);
        }
        list = list->next;
    }
    printf("\nLength = %li\n", length);
}

struct SLNode *addSLNode(struct SLNode *head, struct SLNode *node)
{
    if (head == NULL)
    {
        head = node;
    }
    else
    {
        node->next = head;
        head = node;
    }
    return head;
}

struct DLNode
{
    int data;
    struct DLNode *next;
    struct DLNode *prev;
};

struct DLNode *createDLNode(int data)
{
    struct DLNode *node = malloc(sizeof(struct DLNode));
    node->data = data;
    node->next = NULL;
    node->prev = NULL;
    return node;
}

int sizeDL(struct DLNode *list)
{
    int count = 0;
    while (list != NULL)
    {
        count++;
        list = list->next;
    }
    return count;
}

void printDLList(struct DLNode *list)
{
    printf("List :");
    size_t length = sizeDL(list);
    while (list != NULL)
    {
        if (list->next != NULL)
        {
            printf("%i->", list->data);
        }
        else
        {
            printf("%i", list->data);
        }
        list = list->next;
    }
    printf("\nLength = %li\n", length);
}

struct DLNode *searchMiddleOfList(struct DLNode *head)
{
    struct DLNode *tmp = head;
    struct DLNode *middle = head;
    int count = 1;
    while (tmp != NULL)
    {
        if (count % 2 == 0)
        {
            middle = middle->next;
        }
        tmp = tmp->next;
        count++;
    }
    return middle;
}

struct DLNode *searchMinimalElement(struct DLNode *list)
{
    int k = -1;
    struct DLNode *minimal;
    struct DLNode *middle;
    middle = searchMiddleOfList(list);
    while (middle != NULL)
    {
        if ((k == -1) || (minimal->data > middle->data))
        {
            minimal = middle;
            k++;
        }
        middle = middle->next;
    }

    return minimal;
}

struct DLNode *searchLastElement(struct DLNode *list)
{
    if (list == NULL)
    {
        return NULL;
    }
    struct DLNode *node = list;
    while (node->next != NULL)
    {
        node = node->next;
    }
    return node;
}

struct DLNode *addDLNode(struct DLNode *head, struct DLNode *node)
{
    if (head != NULL)
    {
        struct DLNode *tmp = head;
        if (sizeDL(head) == 1)
        {
            node->next = head;
            head->prev = node;
            head = node;
        }
        else
        {
            struct DLNode *min = searchMinimalElement(head);
            if (min == searchLastElement(head))
            {
                struct DLNode *dodat = min;
                dodat->next = node;
                node->prev = dodat;
            }
            else
            {
                min->next->prev = node;
                node->prev = min;
                node->next = min->next;
                min->next = node;
            }
        }
        return head;
    }
    else
    {
        return NULL;
    }
}

void deleteDLNode(struct DLNode *node)
{
    free(node);
}

struct DLNode *removeDLNode(struct DLNode *head)
{
    if (head != NULL)
    {
        struct DLNode *tail = searchLastElement(head);
        struct DLNode *count = head;
        while (count->next != NULL)
        {
            if (count->data <= tail->data)
            {
                if (count != head)
                {
                    struct DLNode *temp = createDLNode(count->data);
                    temp = count;
                    count->prev->next = count->next;
                    count->next->prev = count->prev;

                    deleteDLNode(temp);
                }
                else
                {
                    struct DLNode *temp = createDLNode(head->data);
                    temp = head;
                    head = head->next;
                    head->prev = NULL;

                    deleteDLNode(temp);
                }
            }
            count = count->next;
        }
        return head;
    }
    else
    {
        return NULL;
    }
}

struct SLNode *createSecondList(struct DLNode *Head, struct SLNode *head_S)
{
    if (Head != NULL)
    {
        struct DLNode *tail = searchLastElement(Head);
        struct DLNode *count = Head;
        while (count->next != NULL)
        {
            if (count->data <= tail->data)
            {
                if (head_S == NULL)
                {
                    struct SLNode *p = createSLNode(count->data);
                    head_S = addSLNode(head_S, p);
                }
                else
                {
                    struct SLNode *p = createSLNode(count->data);
                    head_S = addSLNode(head_S, p);
                }
            }
            count = count->next;
        }
    }

    return head_S;
}
void printDoubleNode(struct SLNode *head_S, struct DLNode *head)
{
    if (head_S != NULL)
    {
        printSLList(head_S);
    }
    else
    {
        printf(KRED "SLNode is empty\n" RESET);
    }

    if (head != NULL)
    {
        printDLList(head);
    }
    else
    {
        printf(KRED "DLNode is empty\n" RESET);
    }
}

void freeAllMemberSL(struct SLNode *head)
{
    struct SLNode *node = head;
    while (node != NULL)
    {
        struct SLNode *next = node->next;
        free(node);
        node = next;
    }
}

void freeAllMemberDL(struct DLNode *head)
{
    struct DLNode *node = head;
    while (node != NULL)
    {
        struct DLNode *next = node->next;
        free(node);
        node = next;
    }
}

int main()
{

    bool exit = false;
    bool list_exist = false;
    int Choosing = -1;
    struct DLNode *head = NULL;
    struct SLNode *head_S = NULL;
    while (exit != true)
    {
        printf("-----------------------------------------\n");
        printf("| ");
        printf(KCYN "Choose one of this action\n" RESET);
        printf("| Press (0) to exit\n");
        printf("| Press (1) to create new list(DLNode)\n");
        printf("| Press (2) to add the element\n");
        printf("| Press (3) to remove element\n");
        printf("------------------------------------------\n");

        scanf("%i", &Choosing);
        if (Choosing == 1)
        {
            if (!list_exist)
            {
                int data;
                printf(KGRN "Please enter the number:\n" RESET);
                scanf("%i", &data);
                head = createDLNode(data);
                printDoubleNode(head_S, head);
                list_exist = true;
            }
            else
            {
                printf(KRED "List already exists\n" RESET);
            }
        }
        else if (Choosing == 2)
        {
            if (list_exist)
            {
                int data;
                printf(KGRN "Please enter the number:\n" RESET);
                scanf("%i", &data);
                struct DLNode *node = createDLNode(data);
                head = addDLNode(head, node);
                printDoubleNode(head_S, head);
            }
            else
            {
                printf(KRED "List doesn't exists\n" RESET);
            }
        }
        else if (Choosing == 3)
        {
            head_S = createSecondList(head, head_S);
            head = removeDLNode(head);
            printDoubleNode(head_S, head);
        }
        else if (Choosing == 0)
        {
            freeAllMemberDL(head);
            freeAllMemberSL(head_S);
            printf(KGRN "You choose (exit)\nI was very happy to work with you\n" RESET);
            exit = true;
        }
        else
        {
            printf(KRED "ERROR.We don't know about this comand.Try to choose again\n" RESET);
        }
    }
}